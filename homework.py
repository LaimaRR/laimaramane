#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import seaborn as sns

from sklearn.preprocessing import scale
from sklearn.metrics import mean_squared_error, r2_score

import statsmodels.api as sm
import statsmodels.formula.api as smf

get_ipython().run_line_magic('matplotlib', 'inline')





df = pd.read_csv(r"C:\Users\Agne\Downloads\pokemon.csv")




df.head()





df.columns




df[['name', 'generation','base_total','hp']].sort_values('base_total', ascending=True).max()




df[['name', 'generation','base_total','hp']].sort_values('hp', ascending=True).max()




df[['name', 'generation','base_total','hp']].sort_values('base_total', ascending=True).iloc[0]




for Name in df:
    listas = []
    if [Legendary[[Legendary] is True]]:
        listas.append(Name)
print(listas)




columns = list(df)
for column in columns:
    print (df[column][100]) # print the third element of the column




df['base_total'].describe()





my_data = ['hp','base_total', 'generation', 'speed', 'base_happiness']
df[my_data].corr()

#df['hp', 'base_total'].corr()




df[['hp','base_total']].corr()



#sns.heatmap(df.corr())


my_data = ['hp','base_total', 'generation', 'speed', 'base_happiness']
sns.heatmap(df[my_data].corr())
#klaidingai:    sns.heatmap(df.corr(my_data))


sns.pairplot(df[['hp','base_total', 'generation', 'speed', 'base_happiness']])


sns.pairplot(df)


sns.distplot(df['base_total'])

sns.distplot(df['hp'])


sns.distplot(df['base_happiness'])



sns.distplot(df['speed'])



sns.lmplot(x='base_total', y='hp', data=df)




sns.lmplot(x='hp', y='base_total', data=df)



from sklearn.model_selection import train_test_split

x = df[['base_total']]
y = df['hp']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4, random_state=101)

train_set = x_train.copy()
train_set['hp'] = y_train


#y.drop(std_res[np.abs(std_res['standard_resid']) > 3].index)



est = smf.ols('hp ~ base_total', train_set).fit()

pred = est.get_prediction(x_test)
pred.summary_frame()



#
# Liekamųjų paklaidų analizė
#


# Residual Standard Error
print('RSE:', np.sqrt(est.scale))
# Mean squared error (MSE) of the residuals. The sum of squared residuals divided by the residual degrees of freedom.
# MSE is a measure of the quality of an estimator—it is always non-negative, and values closer to zero are better.
print('MSE:', est.mse_resid)
# Root mean square error
# RMSD is a measure of accuracy, to compare forecasting errors of different models for a particular dataset and not between datasets, as it is scale-dependent.
print('RMSE:', np.sqrt(est.mse_resid))
print('RMSE:', np.sqrt(sum(pow(est.resid, 2)) / (len(est.resid) - 2) ))

# Residual sum of squares (RSS) arba sum of squared residuals (SSR) arba sum of squared errors of prediction (SSE)
# A small RSS indicates a tight fit of the model to the data
print('RSS/SSR/SSE:', est.ssr)

from sklearn import metrics

prediction = est.predict(x_test)
print('RMSE:', np.sqrt(metrics.mean_squared_error(y_test, prediction)))



#
# Grafiniai metodai
#

from statsmodels.stats.outliers_influence import OLSInfluence
std_res = OLSInfluence(est).summary_frame()

# Standartizuotų liekanų (liekamųjų paklaidų) distribucija. Jeigu forma nėra normalinė - modelis netinka populiacijai
sns.distplot(std_res["standard_resid"], bins=20)



# Standartizuotų liekanų sklaidos diagrama
sns.relplot(data=std_res["standard_resid"])


#
# Išskirtys
#
# Randamos išskirtys; kuomet standartizuota liekana viršyja tris standartinius nuokrypius
std_res[np.abs(std_res['standard_resid']) > 3]



# Randamos išskirtys; Kuko matas (atstumo statistika) - bendru atveju, "iš akies", išskirtis kai > 1
std_res[std_res["cooks_d"] > 1]


#
# Determinacijos koeficientas r^2
#

est.summary()


#
# Regresinės analizės statistinės išvados
#

# Parametrų pasikliautinieji intervalai
est.conf_int(alpha=0.05)


# Hipotezė apie koeficiento b lygybę nuliui (priklausomybė tarp Y ir X)
est.pvalues

# Durbino-Vatsono kriterijus. Kartais prognozės liekamosios paklaidos būna priklausomos. Pavyzdžiui, akcijų kursas
# kinta nevisiškai atsitiktinai. Durbino-Vatsono 1 kriterijus plačiai taikomas laiko
# eilučių teorijoje, kai norima nustatyti, ar yra vadinamasis autoregresijos modelis. Tuo
# tarpu mus domina, ar autoregresijos nėra.

# Hipotezė H0 atmetama (taigi auto-koreliacija yra), jeigu d < di arba d > 4 — di. 
# Hipotezė H0 neatmetama (liekanų autokoreliacija statistiškai nereikšminga), jeigu du < d < 4 — du. 
# Jokių statistinių išvadų negalima padaryti, kai d ι ^ d < du arba 4 — du ^ d ^ 4 — d

# Kuo arčiau 2 - tuo labiau tikėtina autokoreliacijos nėra
est.summary().tables[2]

#
# Prognozavimas regresinėje analizėje
#

# Reikšmių prognozavimas

# Regresinėje analizėje prognozės daromos tik toms χ reikšmėms, kurios patenka į
# duomenų intervalą, t. y. tik tada, kai min(X1 , . . . , Xn ) <= x <= max(X1 , . . . , Xn ) .


# Prognozės intervalas ir Vidutinės prognozuojamos reikšmės pasikliautinasis intervalas
# ci - confidence interval
# ci for mean is the confidence interval for the predicted mean (regression line), ie. for x dot params where the uncertainty is from the estimated params.
# ci for an obs combines the ci for the mean and the ci for the noise/residual in the observation, i.e. it is the confidence interval for a new observation, i.e. ci for x dot params + u which combines the uncertainty coming from the parameter estimates and the uncertainty coming from the randomness in a new observation.
pred.summary_frame(alpha=0.05)

prediction = est.predict(x_test)
sns.distplot((y_test - prediction), bins=20)

fig, axs = plt.subplots(ncols=3)
fig.axes[0].set_ylim(-110, 110)
fig.axes[1].set_ylim(-5, 5)
fig.axes[2].set_ylim(-110, 110)

# Estimator
sns.relplot(data=est.resid, ax=axs[0])
# Standard residuals
sns.relplot(data=std_res["standard_resid"], ax=axs[1])
# Predictor
sns.relplot(data=y_test - prediction, ax=axs[2])



sns.scatterplot(x=y_test, y=prediction)



scat = sns.scatterplot(x=y_test, y=prediction)
scat.axes.set_xlim(0, 250)
scat.axes.set_ylim(0, 250)



axes = scat.get_axes()
axes.set_ylim(0, 250)


est.predict(pd.DataFrame({"hp": [100], "base_total": [500]}))





est.get_prediction(pd.DataFrame({"hp": [100], "base_total": [500]})).summary_frame()




# Atmetam išskirtis ir skaičiuojam iš naujo
from sklearn.model_selection import train_test_split
from statsmodels.stats.outliers_influence import OLSInfluence


est = smf.ols('twd_m2 ~ house_age + convenience_stores + dist_to_nearest_metro_station', df).fit()
std_res = OLSInfluence(est).summary_frame()

df_filtered = df.drop(std_res[np.abs(std_res['standard_resid']) > 3].index)


x = df_filtered[['house_age', 'convenience_stores', 'dist_to_nearest_metro_station']]
y = df_filtered['twd_m2']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4, random_state=101)

train_set = x_train.copy()
train_set['twd_m2'] = y_train

est = smf.ols('twd_m2 ~ house_age + convenience_stores + dist_to_nearest_metro_station', train_set).fit()
std_res = OLSInfluence(est).summary_frame()




est.summary()


print('RMSE:', np.sqrt(est.mse_resid))



est.summary().tables[2]




fig, axs = plt.subplots(ncols=3)
fig.axes[0].set_ylim(-110, 110)
fig.axes[1].set_ylim(-5, 5)
fig.axes[2].set_ylim(-110, 110)

# Estimator
sns.relplot(data=est.resid, ax=axs[0])
# Standard residuals
sns.relplot(data=std_res["standard_resid"], ax=axs[1])
# Predictor
sns.relplot(data=y_test - prediction, ax=axs[2])



est.get_prediction(pd.DataFrame({"hp": [100], "base_total": [500]})).summary_frame()


est.get_prediction(pd.DataFrame({"hp": [255], "base_total": [780]})).summary_frame()


est.resid
